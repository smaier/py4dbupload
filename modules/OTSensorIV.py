#!/usr/bin/python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 22-January-2024

# This module contains the classes that produce the XML file for uploading
# the OT Module Metrology data.

from BaseUploader import ConditionData
from shutil       import copyfile
from copy         import deepcopy
from Utils        import search_files, check_ot_module_label
from Exceptions   import *


class OTSensorIV(ConditionData):
   """Produces the XML file to upload OT Module Metrology data."""

   summary_data   = {'cond_name' : 'Tracker Strip-Sensor Summary Data',
                     'table_name': 'TEST_SENSOR_SMMRY',                   
                     'DBvar_vs_TxtHeader' : {
                                 'AV_TEMP_DEGC'     : 'AV_TEMP_DEGC',
                                 'AV_RH_PRCNT'      : 'AV_RH_PRCNT',
                                 'RMS_FORCE_MEASURED_G' : 'Not in txt file',
                                 'RMS_FORCE_CORRECTED_G' : 'Not in txt file'
                                            }
                    }

   data_description  = {'cond_name'    : 'Tracker Strip-Sensor IV Test',
                        'table_name'   : 'TEST_SENSOR_IV',
                        'DBvar_vs_TxtHeader' : {
                                             'VOLTS'  : 'VOLTS',
                                             'CURRNT_NAMP': 'CURRNT_NAMP',
                                             'TEMP_DEGC' : 'TEMP_DEGC',
                                             'RH_PRCNT'  : 'RH_PRCNT',
                                             'TIME'      : 'TIME'},
                               'mandatory' : ['VOLTS','CURRNT_NAMP']}

   def __init__(self, pDataType, pRunDataReader, pSummaryDataReader, pMeasurementDataReader=None):

      """Constructor: it requires configuration of the upload and the measurement data
      pDataType:
         SUMMARY or DATA depending which block of the xml file is generated
      pRunDataReader:
         NameLabel
         Date
         Commen
         Location
         Inserter
         RunType
      pMeasurementDataReader: List of
         VOLTS
         CURRNT_NAMP
         TEMP_DEGC (optional)
         RH_PRCNT (optional)
         TIME (optional)
      """

      self.type   = pDataType
      sensor_name = pRunDataReader.getDataFromTag('#NameLabel')
      location    = pRunDataReader.getDataFromTag('#Location')
      comment     = pRunDataReader.getDataFromTag('#Comment')
      inserter    = pRunDataReader.getDataFromTag('#Inserter')
      date        = pRunDataReader.getDataFromTag("#Date")
      run_type    = pRunDataReader.getDataFromTag("#RunType")
      kind_of_part= pRunDataReader.getDataFromTag("#KindOfPart")
      configuration = {}
      data_description = self.data_description

      configuration.update(deepcopy(data_description))
      configuration['kind_of_part']     = kind_of_part
      configuration['inserter']     = inserter
      configuration['name_label']       = sensor_name
      configuration['run_comment']  = comment
      configuration['data_comment'] = comment
      configuration['run_location'] = location
      configuration['run_begin']    = date
      configuration['run_type']     = run_type
      configuration['data_version'] = "1.0"
      configuration['attributes']   = [['Data Quality','Good']]  

      #print(configuration)
      if self.type == "SUMMARY":
         name = 'SUMMARY-{}'.format(sensor_name)
         configuration.update(self.summary_data)
         ConditionData.__init__(self, name, configuration, pSummaryDataReader)

         #Check if the data provided could cause harm during upload
         self.do_checks(sensor_name, location)

      if self.type == "DATA":
         name = 'DATA-{}'.format(sensor_name)
         configuration.update(self.data_description)
         ConditionData.__init__(self, name, configuration, pMeasurementDataReader)
      
   def do_checks(self, sensor_name, location):
      #Check Block

      #Check if the module exists in the DB
      if self.db.component_id(sensor_name) is None:
         print("Error - Module not in DB!")
         exit(1)
      else:
         print("Module part id: checked")


      location_id_of_location = self.db.get_location_id(location)

      #Check if the location name is valid
      if location_id_of_location is None:
         print("Error - location does not exist")
         exit(1)
      else:
         print("Location: checked")

      #Check if the measurement is at the same location as the component is
      location_id_of_component = self.db.component_location(sensor_name)
      print(location_id_of_location)
      print(location_id_of_component)
      if location_id_of_location != location_id_of_component:
         print("Error - Location of measurement does not match location of coponentn in DB")
         exit(1)
      else:
         print("Location of component matches: checked")
