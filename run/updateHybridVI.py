#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 02-August-2024

# This script masters the insertion of the VI result for Hybrid components.

import os,time,datetime

from AnsiColor    import Fore, Back, Style
from DataReader   import TableReader
from Hybrid       import HybridComponentT,HybridResultsT
from UpdatePart   import Update
from BaseUploader import BaseUploader
from Utils        import UploaderContainer,DBupload,DBaccess,search_files
from optparse     import OptionParser
from progressbar  import *


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the txt files with measurement data.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')
   
   (opt, args) = p.parse_args()

   if len(args)>2:
      p.error("accepts at most one argument!")


   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'

   DB = DBaccess(database   = f'trker_{BaseUploader.database}',\
                 login_type = BaseUploader.login,\
                 verbose    = BaseUploader.debug)
   
   # Request access token
   DB.component_location('test')
   
   hybrids = UploaderContainer('Hybrids')
   results = UploaderContainer('VisualInspectionData')

   # gather excel files containing the data measurements
   data_files = args[1] if len(args)==2 else \
                sorted( search_files(opt.data_path,'*.csv') )


   nFiles = len(data_files)
   bar = progressbar(range(nFiles), widgets=['Processing csv files: ', \
                Bar('=', '[', ']'), ' ', Percentage()])

   for f in data_files:
      sd = TableReader(f,csv_delimiter=",")      
      data = sd.getDataAsCWiseDictRowSplit()
      
      wig = [f'Processing {os.path.basename(f)}: ', Bar('=', '[', ']'), ' ', Percentage()]
      for i in progressbar(range(len(data)), widgets=wig, redirect_stdout=True):
         cd = data[i]
         serial = cd['Serial number']
         location = DB.component_location(id=serial,idt='serial_number')
         
         # Update data only for registered hybrids
         if location != None:
            qy=f'select l.location_name from {DB.database}.trkr_locations_v l where l.location_id={location}'
            location_name = DB.data_query(qy)[0]['locationName']
            hybrid = HybridComponentT(serial,location_name)
            
            # Update the Hybrids attributes
            visual = 'Good' if cd['Hybrid status']=='Accepted' else 'Bad'
            status = hybrid.component_status_from_tests(visual_status=visual)
            hybrid_qualification = {
               'attributes'   : [('Status',status),('Visual inspection status',visual)]
                                   }
            Utemplate = Update(serial,Idt='serial_number',\
                               default=hybrid_qualification,mode='batch')
            hybrids.add(Utemplate)
            
            # Update the Hybrids Visual Inspection data
            #timestamp = datetime.datetime.strptime(cd['Date'], '%Y-%m-%d %H:%M:%S')
            try:
                check = time.strptime(cd['Date'][:8], '%H:%M:%S')
                timestamp = datetime.datetime.strptime(cd['Date'], '%Y-%m-%d %H:%M:%S')
            except ValueError:
                timestamp = datetime.datetime.strptime(cd['Date']+' 00:00:00', '%Y-%m-%d %H:%M:%S')
            #   timestamp += ' 00:00:00'               
               
            Rtemplate = HybridResultsT(serial,'Visual')
            Rtemplate.load_run_metadata(run_type     = 'VI', 
                                        run_begin    = timestamp,
                                        run_operator = cd['Person responsible for the VI'],
                                        run_location = 'CERN') 
                                        #run_comment  = self.global_run_comment)
            Rtemplate.load_visual_data(cd['Folder'])
            results.add(Rtemplate)

            
         print(f'{serial} processed!')
         time.sleep(0.1)

   streams.flush()

   hybrid_xml = hybrids.dump_xml_data()
   result_xml = results.dump_xml_data(pSkipPartsBlock=True)
   
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                      login_type=BaseUploader.login,verbose=opt.debug)
   
   if opt.upload:
      db_loader.upload_data(hybrid_xml)
      db_loader.upload_data(result_xml)
