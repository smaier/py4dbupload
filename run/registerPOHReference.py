#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 30-March-2021

# This script masters the registration of the Reference cuts for the POH 
# acceptance test


import traceback,sys
import pandas as pd

from AnsiColor    import Fore, Back, Style
from Utils        import *
from BaseUploader import *
from Hybrid       import ReferenceT, ReferenceCutsT
from optparse     import OptionParser
from datetime     import date, datetime
from os           import path,environ,remove

def is_unique(s):
    a = s.to_numpy()
    return (a[0] == a).all()


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <serial number file>, ...", version="1.1")

   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')

   p.add_option( '--date',
               type    = 'string',
               default = '',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the registration inserted in the run metadata. If not given the date of today is used.')

   p.add_option( '-o','--operator',
               type    = 'string',
               default = '',
               dest    = 'operator',
               metavar = 'STR',
               help    = 'The operator that performed the run metadata insertion.')
   
   p.add_option( '-i','--inserter',
               type    = 'string',
               default = '',
               dest    = 'inserter',
               metavar = 'STR',
               help    = 'The inserter pseudonimus for shadowing the account name in the database.')
  
   p.add_option( '-v',
               type    = 'string',
               default = '',
               dest    = 'version',
               metavar = 'STR',
               help    = 'The version tag of the reference values. if given overwrite the version tag in the reference files.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'perform the data uopload thorugh cmsdbloader api.')
   
   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)!=1:
      p.error('need the directory containing the reference file as argument!')


   if opt.verbose:  BaseUploader.verbose = True

   
   database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   db = DBaccess(database=f'trker_{database}', verbose=opt.debug)

   date = opt.date if opt.date!='' else date.today()
   

   if opt.verbose:
      print(f'Looking for reference files into {args[0]}\n')

   reference_files = search_files(top_level_path=args[0], pattern='*.csv')
   reference_kinds = db.data_query(f'select * from trker_{database}.trkr_kinds_of_references_v r')
   reference_tags  = [x['kindOfReferenceId'] for x in reference_kinds if 'POH' in x['commentDescription'] ]
   to_be_processed = []

   for t in reference_tags:
      import re
      found = [ f for f in reference_files if re.search(t+'_',f,re.IGNORECASE)]
      if len(found) > 1:
         print(f'Found more tha  1 file for tag {t}; process is terminating ....')
         exit(1)
      if len(found) == 0:
         print(f'No reference file found for tag {t}; process is terminating ....')
         exit(1)
      to_be_processed.append((t,found[0]))
   
   if opt.verbose:
      print("Building acceptance reference cuts from the following files:")
      for c in to_be_processed:
         print('  {0: <10}{1}'.format(c[0],c[1]))
   
   ref_list = []
   for c in to_be_processed:
      df = pd.read_csv(c[1]).assign(KIND_OF_REFERENCE_ID = c[0])
      if opt.version!='':
         df.drop(labels='VERSION', axis='columns', inplace=True)
         df['VERSION'] = opt.version
      if opt.debug: 
         print(f'====== {c[1]} ======')
         print(df.to_string())
      ref_list.append(df)
   
   ref_merged = pd.concat(ref_list, ignore_index=True)

   if not is_unique( ref_merged['VERSION'] ):
      print(f'No unique version in the reference; process is terminating ....')
      exit(1)
   
   version = ref_merged['VERSION'][0]
   name = f'POH_{version}_ref'
   
   ref_merged.to_csv(f'{name}.csv', index=False)   
   
   run_data = {
      'run_type'         : 'Ref_Hyb_Acc',
      'run_begin'        : datetime.strftime(date,'%Y-%m-%d'),
      'run_comment'      : 'Test for development',
      'run_operator'     : opt.operator,
      'run_location'     : 'CERN',
   }
   
   ref_uploader = ReferenceCutsT(name,run_data,ReferenceT.POH_acc.name, inserter=opt.inserter)
   ref_uploader.dump_xml_data()
   
   # zip output files
   file_list = [f'{name}.csv',f'{name}.xml']
   from zipfile import ZipFile
   zipfilename = f'{name}.zip'
   with ZipFile(zipfilename, 'w') as (zip):
      for f in file_list: zip.write(f)
      for f in file_list: remove(f)
      
   db_loader = DBupload(database=f'{database}',\
                        path_to_dbloader_api=path.dirname(environ.get('DBLOADER')),\
                        verbose=opt.verbose)
   if opt.upload:
      db_loader.upload_data(zipfilename)
   