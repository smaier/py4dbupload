#!/usr/bin/env python
# $Id$
# Created by Stefan Maier  <stefan.maier@cern.ch>, 18.10.20

# This script corrects the faulty serial numbers of MPA Chips.

import os,time,datetime, csv

from AnsiColor    import Fore, Back, Style
from DataReader   import TableReader
from Hybrid       import HybridComponentT,HybridResultsT
from MPA          import MPAchipUpdate
from Query        import Query
from BaseUploader import BaseUploader
from Utils        import UploaderContainer,DBupload,DBaccess,search_files
from optparse     import OptionParser
from progressbar  import *


def generate_mpa_label (lot_name , wafer_number,  pos_row, pos_col):
    pm = "1" if pos_col < 0 else "0"
    name = lot_name + "_" + "{:02d}".format(wafer_number) + "_" + "{:01X}".format(pos_row) + pm + "{:01X}".format(abs(pos_col))
    return name


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the txt files with correction data.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')
   
   (opt, args) = p.parse_args()

   db = 'trker_cmsr'
   if opt.isDevelopment:
      db = 'trker_int2r'

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'


   corr = []
   lot = ""
   wafer =""
   print("Loaded correction file")
   with open(opt.data_path) as csvfile:
      spamreader = csv.reader(csvfile)
      for i, row in enumerate(spamreader):
         if i > 0:
            #print(row)
            name = generate_mpa_label(row[0], int(row[2]), int(row[3]), int(row[4]))
            corr.append({"name" : name, "new_serial": str(row[7])})
            lot = row[0]
            wafer = row[2]

   print("Search DB for MPAs of given wafer")
   query = Query(db,'login' if not opt.twofa else 'login2', opt.verbose)
   data = query.getMpaIdsOfWafer(lot, wafer)
   print("Found MPAs of wafer ", lot, wafer)
   [print(point) for point in data]
   match = 0

   print("Search for matches")
   for corr_item in corr:
      for db_entry in data:
         if db_entry["nameLabel"] == corr_item["name"]:
            corr_item["id"] = db_entry["id"]
            corr_item["old_serial"] = db_entry["serialNumber"]
            print("found match ", corr_item )
            match += 1

   if match ==len(data):
      print("Found all MPAs of wafer")
   else:
      print("Number of MPAs in file ({}) is not the same as MPAs found in the DB ({})".format(match, len(data)))
      exit(1)

   mpa = None
   update_serials_file = None
   mpa_update = UploaderContainer('MPAUpdate')
   for corr_item in corr:
      mpa = MPAchipUpdate({},{'serial_number':corr_item["new_serial"],'id':corr_item['id'] })
      mpa_update.add(mpa)

   file = mpa_update.dump_xml_data()

   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                        login_type=BaseUploader.login,verbose=True)
   if opt.upload:
      print("Upload correction")
      db_loader.upload_data(file)
