#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 30-Nov-2019

# This script masters the insertion of the updates.

import os,sys,fnmatch

from AnsiColor  import Fore, Back, Style
from Exceptions import *
from UpdatePart import *
from optparse   import OptionParser


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options]", version="1.1")

   p.add_option( '-k','--kop',
                  type    = 'string',
                  default = '',
                  dest    = 'kind_of_part',
                  metavar = 'STR',
                  help    = 'Kind of Part to be updated')

   p.add_option( '--id',
                  action  = 'append',
                  type    = 'string',
                  default = [],
                  dest    = 'ids',
                  metavar = 'STR',
                  help    = 'ID of the components to be modified')

   p.add_option( '--idtype',
                  type    = 'string',
                  default = 'serial',
                  dest    = 'idtype',
                  metavar = 'STR',
                  help    = 'Type of ID: \'serial\', \'barcode\', \'name_labe\'')

   p.add_option( '-v','--ver',
                  type    = 'string',
                  default = '',
                  dest    = 'version',
                  metavar = 'STR',
                  help    = 'Version of the components')

   p.add_option( '-d','--desc',
                  type    = 'string',
                  default = '',
                  dest    = 'description',
                  metavar = 'STR',
                  help    = 'Input a description for the components to be updated')

   p.add_option( '--verbose',
                  action  = 'store_true',
                  default = False,
                  dest    = 'verbose',
                  help    = 'Force the uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)!=0:
      p.error('no argument is needed!')

   if len(opt.ids)==0:
      p.error('at least one serial number must be entered!')

   if opt.kind_of_part=='':
      p.error('kind_of_part must be specified!')

   # configuration dictionary storing the pair <key:value> to be updated
   conf = {}
   conf['kind_of_part'] = opt.kind_of_part   # specify the kind of part

   if opt.description!='':
      conf['description'] = opt.description  # register a component description
   if opt.version!='':
      conf['version']     = opt.version      # register a component version 

   #update attributes (must be specified in the script)
   #conf['attributes'] = [('Status','Good')]

   #update serial_number
   #conf['serial_number'] = 'XXXYYYZZZ'

   #update barcode
   #conf['barcode'] = 'XXXYYYZZZ'

   #update name_label
   #conf['name_label'] = 'XXXYYYZZZ'
  
   if opt.verbose:  BaseUploader.verbose = True

   update = Update('update',conf,opt.ids,opt.idtype)
  
   update.dump_xml_data()

