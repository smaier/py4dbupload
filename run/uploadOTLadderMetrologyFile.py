#!/usr/bin/env python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 22-January-2024

# This script masters the upload of OT module metrology data.

#import os,sys,fnmatch

#from AnsiColor  import Fore, Back, Style
#from Exceptions import *
from Utils        import search_files,DBupload,UploaderContainer
from DataReader   import TableReader
from BaseUploader import BaseUploader
from OTLadderMetrologyFile import OTLadderMetrologyFile
from datetime     import date
from dateutil     import parser
from optparse     import OptionParser
from progressbar  import *

import os,time, yaml, copy

if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <measurement data file> ", version="1.1")

   p.add_option( '--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the tables with the measurement data.')

   p.add_option( '--file',
               type    = 'string',
               default = None,
               dest    = 'metrology_file',
               metavar = 'STR',
               help    = 'Path to the root file to be uploaded.')

   p.add_option( '-f', '--format',
                type = 'string',
                default = None,
                dest    = 'format',
                metavar = 'STR',
                help    = 'Specifiy which data input format from which site is given. E.g. KIT, AAC, BRN, etc...')

   p.add_option( '-c', '--config',
                type = 'string',
                default = None,
                dest    = 'config_file',
                metavar = 'STR',
                help    = 'Configuration file containing information about inserter, location, etc')

   p.add_option( '-i','--inserter',
               type    = 'string',
               default = None,
               dest    = 'inserter',
               metavar = 'STR',
               help    = 'Overwirtes the account name put in the RECORD_INSERTION_USER column.')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--update',
               action  = 'store_true',
               default = False,
               dest    = 'update',
               help    = 'Force the upload of components in update mode')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database.')
   
   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data.')

   p.add_option( '--store',
               action  = 'store_true',
               default = False,
               dest    = 'store',
               help    = 'Store the generated XML/ZIP file. Otherwise, delete it immediately')

   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   (opt, args) = p.parse_args()

   if len(args)>1:
      p.error('accepts at most 1 argument!')

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'
   
   module_test_data_file  = opt.data_path

   files_to_be_uploaded = []

   ladder_metrology_test_runinfo_reader   = TableReader(module_test_data_file, d_offset=0, m_rows=5, csv_delimiter=',', tabSize=23)
   ladder_metrology_test_data_reader      = TableReader(module_test_data_file, d_offset=7, m_rows=2, csv_delimiter=',', tabSize=30)

   if ladder_metrology_test_data_reader.getDataAsCWiseDictRowSplit()[0].get("METROLOGY_FILE","") !="":
      metrologyFile = os.path.dirname(module_test_data_file) + '/' + ladder_metrology_test_data_reader.getDataAsCWiseDictRowSplit()[0]["METROLOGY_FILE"]
   else:
      print("No root file given in file, abort")
      exit(0)
   #Give option that the root file is not in the same folder as the csv file. will overwrite the roo File path
   if opt.metrology_file:
      metrologyFile = opt.metrology_file

   OT_Ladder_Metrology_File_Container = UploaderContainer(os.path.dirname(module_test_data_file) + '/' +'OTLadderMetrologyFile')

   print ('\n\Run info data to upload',   ladder_metrology_test_runinfo_reader)
   print ('\n\Module test data to upload',ladder_metrology_test_data_reader)

   #Get the three data components from the csv file
   SMMRY  = OTLadderMetrologyFile(ladder_metrology_test_runinfo_reader, ladder_metrology_test_data_reader)
   OT_Ladder_Metrology_File_Container.add(SMMRY)
   time.sleep(0.1)

   streams.flush()
   

   xmlFile = OT_Ladder_Metrology_File_Container.dump_xml_data(pSkipPartsBlock = True)
   zipfilename = os.path.dirname(module_test_data_file) + '/' + 'OTLadderMetrologyFile.zip'

   from zipfile import ZipFile
   
   with ZipFile(zipfilename, 'w') as (zip):
      zip.write(xmlFile,os.path.basename(xmlFile))
      zip.write(metrologyFile,os.path.basename(metrologyFile))

   files_to_be_uploaded.append(zipfilename)

   # Upload files in the database 
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path ,verbose=True)
   if opt.upload:  
      for fi in files_to_be_uploaded:
         # Files upload
         db_loader.upload_data(fi)

   if not opt.store:
      for fi in files_to_be_uploaded:
         print("Remove temporary files")
         os.remove(fi)
      os.remove(xmlFile)