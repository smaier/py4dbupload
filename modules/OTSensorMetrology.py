#!/usr/bin/python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 22-January-2024

# This module contains the classes that produce the XML file for uploading
# the OT Module Metrology data.

from BaseUploader import ConditionData
from shutil       import copyfile
from copy         import deepcopy
from Utils        import search_files, check_ot_module_label
from Exceptions   import *


class OTSensorMetrology(ConditionData):
   """Produces the XML file to upload OT Module Metrology data."""

   summary_data   = {'cond_name' : 'Tracker Module Assembly Summary Data',
                     'table_name': 'TEST_ASSEMBLY_SMMRY',                   
                     'DBvar_vs_TxtHeader' : {
                                 'STATION'          : 'STATION',
                                 'AV_TEMP_DEGC'     : 'Not in txt file',
                                 'AV_RH_PRCNT'      : 'Not in txt file',
                                 'RMS_FORCE_MEASURED_G' : 'Not in txt file',
                                 'RMS_FORCE_CORRECTED_G' : 'Not in txt file'
                                            },
                     'mandatory' : ['STATION']
                    }

   data_description  = {'2S':{    'kind_of_part' : '2S Sensor',     
                            'cond_name'    : 'Tracker Sensor Metrology',
                            'table_name'   : 'TEST_SENSOR_MTRLGY',
                            'DBvar_vs_TxtHeader' : {
                                             'CARDINAL_DIRECTION'  : 'CARDINAL_DIRECTION',
                                             'DISTANCE_LEFT_UM'     : 'DISTANCE_LEFT_UM',
                                             'DISTANCE_RIGHT_UM'     : 'DISTANCE_RIGHT_UM'},
                               'mandatory' : ['CARDINAL_DIRECTION','DISTANCE_LEFT_UM','DISTANCE_RIGHT_UM']
                        },
                        'PS-s':{    'kind_of_part' : 'PS-s Sensor',     
                            'cond_name'    : 'Tracker Sensor Metrology',
                            'table_name'   : 'TEST_SENSOR_MTRLGY',
                            'DBvar_vs_TxtHeader' : {
                                             'CARDINAL_DIRECTION'  : 'CARDINAL_DIRECTION',
                                             'DISTANCE_LEFT_UM'     : 'DISTANCE_LEFT_UM',
                                             'DISTANCE_RIGHT_UM'     : 'DISTANCE_RIGHT_UM'},
                               'mandatory' : ['CARDINAL_DIRECTION','DISTANCE_LEFT_UM','DISTANCE_RIGHT_UM']
                        },
                        'PS-p':{    'kind_of_part' : 'PS-p Sensor',     
                            'cond_name'    : 'Tracker Sensor Metrology',
                            'table_name'   : 'TEST_SENSOR_MTRLGY',
                            'DBvar_vs_TxtHeader' : {
                                             'CARDINAL_DIRECTION'  : 'CARDINAL_DIRECTION',
                                             'DISTANCE_LEFT_UM'     : 'DISTANCE_LEFT_UM',
                                             'DISTANCE_RIGHT_UM'     : 'DISTANCE_RIGHT_UM'},
                               'mandatory' : ['CARDINAL_DIRECTION','DISTANCE_LEFT_UM','DISTANCE_RIGHT_UM']
                        }}
   def __init__(self, pDataType, pRunDataReader, pSummaryDataReader, pMeasurementDataReader=None):

      """Constructor: it requires configuration of the upload and the measurement data
      pDataType:
         SUMMARY or DATA depending which block of the xml file is generated
      pRunDataReader:
         NameLabel
         Date
         Commen
         Location
         Inserter
      pSummaryDataReader
         STATION
      pMeasurementDataReader:
         KIND_OF_METROLOGY:    metrology type
         ROTATION_URAD: Rotation of the sensors
         X_SHIFT_UM:    X shift of the sensors
         Y_SHIFT_UM:    Y shift of the sensors
      """

      self.type   = pDataType
      sensor_name = pRunDataReader.getDataFromTag('#NameLabel')
      location    = pRunDataReader.getDataFromTag('#Location')
      comment     = pRunDataReader.getDataFromTag('#Comment')
      inserter    = pRunDataReader.getDataFromTag('#Inserter')
      date        = pRunDataReader.getDataFromTag("#Date")
      run_type    = pRunDataReader.getDataFromTag("#RunType")
      configuration = {}

      if "2-S" in sensor_name:
         data_description = self.data_description["2S"]
      elif "PSS" in sensor_name:
         data_description = self.data_description["PS-s"]
      elif "PSP" in sensor_name:
         data_description = self.data_description["PS-p"]
      else:
         print("Sensor type not reckognized")
         exit(1)

      configuration.update(deepcopy(data_description))


      configuration['inserter']     = inserter
      configuration['name_label']   = sensor_name
      configuration['run_comment']  = comment
      configuration['data_comment'] = comment
      configuration['run_location'] = location
      configuration['run_begin']    = date
      configuration['run_type']     = run_type
      configuration['data_version'] = "1.0"
      configuration['attributes']   = [['Data Quality','Good']]  

      #print(configuration)
      if self.type == "SUMMARY":
         name = 'SUMMARY-{}'.format(sensor_name)
         configuration.update(self.summary_data)
         ConditionData.__init__(self, name, configuration, pSummaryDataReader)

         #Check if the data provided could cause harm during upload
         self.do_checks(sensor_name, location)

      if self.type == "DATA":
         name = 'DATA-{}'.format(sensor_name)
         configuration.update(self.data_description)
         ConditionData.__init__(self, name, configuration, pMeasurementDataReader)
      
   def do_checks(self, sensor_name, location):
      #Check Block

      #Check if the sensor exists in the DB
      if self.db.component_id(sensor_name) is None:
         print("Error - Sensor not in DB!")
         exit(1)
      else:
         print("Sensor id: checked")

      location_id_of_location = self.db.get_location_id(location)

      #Check if the location name is valid
      if location_id_of_location is None:
         print("Error - location does not exist")
         exit(1)
      else:
         print("Location: checked")


      #Check if the measurement is at the same location as the component is
      location_id_of_component = self.db.component_location(sensor_name)
      #print(location_id_of_location)
      #print(location_id_of_component)
      if location_id_of_location != location_id_of_component:
         print("Error - Location of measurement does not match location of coponentn in DB")
         exit(1)
      else:
         print("Location of component matches: checked")
