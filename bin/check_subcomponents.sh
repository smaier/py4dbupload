#!/bin/bash

if [ $# -ne 3 ] && [ $# -ne 4 ]
then
   echo ""
   echo "Usage: $0 < Kind of part name > < correct number of sub-component > < -s | -n | -b > [connection]"
   echo -e "\t-s use serial number as identifier."
   echo -e "\t-n use name label as identifier."
   echo -e "\t-b use barcode as identifier."
   echo -e "\t-connection:  \"cmsr\" for production, \"int2r\" for development; default is \"cmsr\"."
   echo -e "" 
   echo -e "\texample: $0 \"PS Front-end Hybrid\" 8 "
   echo -e "\tto find in cmsr all the PS Front-end Hybrid which doesn't have 8 children"
   exit 1 # Exit script after printing help
fi

if [ "$3" = "-s" ]
then
   ID='parent_serial_number'
elif [ "$3" = "-n" ]
then
   ID='parent_name_label'
elif [ "$3" = "-b" ]
then
   ID='parent_barcode'
else
   echo "Third option can be only \"-s\" or \"-n\" or \"-b\""
   /bin/bash $0
   exit 1
fi


echo 'Available connections: '
python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login2

DB='trker_cmsr'

if [ -n "$4" ]
then
   DB="trker_$4" 
fi

echo 'Connect to ' ${DB}
echo


python3 ${RHAPI} --f csv --url=https://cmsdca.cern.ch/trk_rhapi --login2 "select r.$ID, count(*) as children from $DB.trkr_relationships_v r where r.parent_component='$1' group by r.$ID " --all -n > result.csv

results="$(head -n 1 result.csv)"
newline=$'\n'

while IFS="," read -r id children
do 
   if [ "$children" != "$2" ] && [ "$id" != "" ]
   then
   results="$results \n$id, $children"
   fi
done < <(tail -n +2 result.csv)

echo -e $results | column -s, -t
