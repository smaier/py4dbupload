08.06.2019 09:35 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.10:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m100[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000066/calibration/Calibration_Electron_08-06-19_09h34/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x23a (570)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m21[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m58[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

08.06.2019 09:35 ||I| Creating directory: Results/IntegratedTester_Electron_08-06-19_09h35
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
08.06.2019 09:35 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
08.06.2019 09:35 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
08.06.2019 09:35 ||I| [1m[32mSetting the I2C address table[0m
08.06.2019 09:35 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
08.06.2019 09:35 ||I| Enabling Hybrid 0
08.06.2019 09:35 ||I|      Enabling Chip 0
08.06.2019 09:35 ||I|      Enabling Chip 1
08.06.2019 09:35 ||I|      Enabling Chip 2
08.06.2019 09:35 ||I|      Enabling Chip 3
08.06.2019 09:35 ||I|      Enabling Chip 4
08.06.2019 09:35 ||I|      Enabling Chip 5
08.06.2019 09:35 ||I|      Enabling Chip 6
08.06.2019 09:35 ||I|      Enabling Chip 7
08.06.2019 09:35 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
08.06.2019 09:35 ||I| Successfully received *Pings* from 8 Cbcs
08.06.2019 09:35 ||I| [32mCBC3 Phase tuning finished succesfully[0m
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| Waiting for DDR3 to finish initial calibration
08.06.2019 09:35 ||I| [32mSuccessfully configured Board 0[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 0[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 1[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 2[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 3[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 4[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 5[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 6[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 7[0m
08.06.2019 09:35 ||I| [32mCalibrating CBCs before starting antenna test of the CBCs on the DUT.[0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:35 ||I| [1m[34m CHIP ID FUSE 399062[0m
08.06.2019 09:35 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:35 ||I| [1m[34m CHIP ID FUSE 399076[0m
08.06.2019 09:35 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:35 ||I| [1m[34m CHIP ID FUSE 399090[0m
08.06.2019 09:35 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:35 ||I| [1m[34m CHIP ID FUSE 464641[0m
08.06.2019 09:35 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:35 ||I| [1m[34m CHIP ID FUSE 464656[0m
08.06.2019 09:35 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:35 ||I| [1m[34m CHIP ID FUSE 464671[0m
08.06.2019 09:35 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:35 ||I| [1m[34m CHIP ID FUSE 464686[0m
08.06.2019 09:35 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:35 ||I| [1m[34m CHIP ID FUSE 464699[0m
08.06.2019 09:35 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| Created Object Maps and parsed settings:
08.06.2019 09:35 ||I| 	Nevents = 100
08.06.2019 09:35 ||I| 	TestPulseAmplitude = 0
08.06.2019 09:35 ||I|   Target Vcth determined algorithmically for CBC3
08.06.2019 09:35 ||I|   Target Offset fixed to half range (0x80) for CBC3
08.06.2019 09:35 ||I| [1m[34mExtracting Target VCth ...[0m
08.06.2019 09:35 ||I| Disabling all channels by setting offsets to 0xff
08.06.2019 09:35 ||I| Setting offsets of Test Group -1 to 0xff
08.06.2019 09:35 ||I| [32mEnabling Test Group....-1[0m
08.06.2019 09:35 ||I| Setting offsets of Test Group -1 to 0x80
08.06.2019 09:35 ||I| [31mDisabling Test Group....-1[0m
08.06.2019 09:35 ||I| Setting offsets of Test Group -1 to 0xff
08.06.2019 09:35 ||I| [1m[32mMean VCth value for FE 0 CBC 0 is [1m[31m594[0m
08.06.2019 09:35 ||I| [1m[32mMean VCth value for FE 0 CBC 1 is [1m[31m588[0m
08.06.2019 09:35 ||I| [1m[32mMean VCth value for FE 0 CBC 2 is [1m[31m591[0m
08.06.2019 09:35 ||I| [1m[32mMean VCth value for FE 0 CBC 3 is [1m[31m599[0m
08.06.2019 09:35 ||I| [1m[32mMean VCth value for FE 0 CBC 4 is [1m[31m593[0m
08.06.2019 09:35 ||I| [1m[32mMean VCth value for FE 0 CBC 5 is [1m[31m609[0m
08.06.2019 09:35 ||I| [1m[32mMean VCth value for FE 0 CBC 6 is [1m[31m601[0m
08.06.2019 09:35 ||I| [1m[32mMean VCth value for FE 0 CBC 7 is [1m[31m593[0m
08.06.2019 09:35 ||I| [1m[34mMean VCth value of all chips is 596 - using as TargetVcth value for all chips![0m
08.06.2019 09:35 ||I| [32mEnabling Test Group....0[0m
08.06.2019 09:35 ||I| Setting offsets of Test Group 0 to 0xff
08.06.2019 09:35 ||I| [31mDisabling Test Group....0[0m
08.06.2019 09:35 ||I| [32mEnabling Test Group....1[0m
08.06.2019 09:35 ||I| No data in the readout after receiving all triggers. Re-trying the point
08.06.2019 09:35 ||I| Setting offsets of Test Group 1 to 0xff
08.06.2019 09:35 ||I| [31mDisabling Test Group....1[0m
08.06.2019 09:35 ||I| [32mEnabling Test Group....2[0m
08.06.2019 09:35 ||I| Setting offsets of Test Group 2 to 0xff
08.06.2019 09:35 ||I| [31mDisabling Test Group....2[0m
08.06.2019 09:35 ||I| [32mEnabling Test Group....3[0m
08.06.2019 09:35 ||I| Setting offsets of Test Group 3 to 0xff
08.06.2019 09:35 ||I| [31mDisabling Test Group....3[0m
08.06.2019 09:35 ||I| [32mEnabling Test Group....4[0m
08.06.2019 09:35 ||I| Setting offsets of Test Group 4 to 0xff
08.06.2019 09:35 ||I| [31mDisabling Test Group....4[0m
08.06.2019 09:35 ||I| [32mEnabling Test Group....5[0m
08.06.2019 09:35 ||I| Setting offsets of Test Group 5 to 0xff
08.06.2019 09:35 ||I| [31mDisabling Test Group....5[0m
08.06.2019 09:35 ||I| [32mEnabling Test Group....6[0m
08.06.2019 09:35 ||I| No data in the readout after receiving all triggers. Re-trying the point
08.06.2019 09:35 ||I| Setting offsets of Test Group 6 to 0xff
08.06.2019 09:35 ||I| [31mDisabling Test Group....6[0m
08.06.2019 09:35 ||I| [32mEnabling Test Group....7[0m
08.06.2019 09:35 ||I| Setting offsets of Test Group 7 to 0xff
08.06.2019 09:35 ||I| [31mDisabling Test Group....7[0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:35 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:35 ||I| [1m[32mApplying final offsets determined by tuning to chip - no re-configure necessary![0m
08.06.2019 09:35 ||I| Results saved!
08.06.2019 09:35 ||I| [1m[34mConfigfiles for all Cbcs written to Results/IntegratedTester_Electron_08-06-19_09h35[0m
08.06.2019 09:35 ||I| Calibration finished.
Calibration of the DUT finished at: Sat Jun  8 09:35:27 2019
	elapsed time: 10.4373 seconds
08.06.2019 09:35 ||I| Starting noise occupancy test.
08.06.2019 09:35 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
08.06.2019 09:35 ||I| [1m[32mSetting the I2C address table[0m
08.06.2019 09:35 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
08.06.2019 09:35 ||I| Enabling Hybrid 0
08.06.2019 09:35 ||I|      Enabling Chip 0
08.06.2019 09:35 ||I|      Enabling Chip 1
08.06.2019 09:35 ||I|      Enabling Chip 2
08.06.2019 09:35 ||I|      Enabling Chip 3
08.06.2019 09:35 ||I|      Enabling Chip 4
08.06.2019 09:35 ||I|      Enabling Chip 5
08.06.2019 09:35 ||I|      Enabling Chip 6
08.06.2019 09:35 ||I|      Enabling Chip 7
08.06.2019 09:35 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
08.06.2019 09:35 ||I| Successfully received *Pings* from 8 Cbcs
08.06.2019 09:35 ||I| [32mCBC3 Phase tuning finished succesfully[0m
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
08.06.2019 09:35 ||I| 		 Mode: 0
08.06.2019 09:35 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:35 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:35 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:35 ||I| Waiting for DDR3 to finish initial calibration
08.06.2019 09:35 ||I| [32mSuccessfully configured Board 0[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 0[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 1[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 2[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 3[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 4[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 5[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 6[0m
08.06.2019 09:35 ||I| [32mSuccessfully configured Cbc 7[0m
08.06.2019 09:35 ||I| Trigger source 01: 7
08.06.2019 09:35 ||I| Histo Map for Module 0 does not exist - creating 
08.06.2019 09:35 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
08.06.2019 09:35 ||I| 7
08.06.2019 09:35 ||I| Mesuring Efficiency per Strip ... 
08.06.2019 09:35 ||I| Taking data with 100 Events!
08.06.2019 09:35 ||I| 		Mean occupancy for the Top side: 61.3448 error= 8.69229[0m
08.06.2019 09:35 ||I| 		Mean occupancy for the Botton side: 62.0108 error= 8.12967[0m
# Noisy channels on Bottom Sensor : 1,2,3,5,9,13,15,17,19,21,25,34,36,40,41,44,45,46,49,51,58,59,60,69,74,75,78,80,89,90,91,93,94,96,102,103,105,108,112,114,115,117,120,121,123,124,125,127,130,131,135,136,139,143,144,147,149,152,154,158,160,163,164,165,167,170,172,173,180,183,184,185,187,188,189,192,193,199,200,209,210,213,219,222,223,224,225,229,230,232,233,234,237,238,241,242,243,245,246,248,252,254,255,259,262,265,266,268,269,282,284,285,286,288,289,291,296,298,301,305,309,311,316,317,323,327,328,329,330,331,332,333,337,341,342,344,351,352,353,354,357,358,362,363,364,365,366,368,369,371,375,376,378,379,381,386,387,388,389,390,394,395,400,401,402,406,407,408,425,426,429,437,440,441,442,448,450,452,454,455,456,457,458,462,463,466,469,476,478,479,480,482,486,488,489,490,493,494,497,500,508,509,510,512,513,515,520,527,528,535,538,539,543,545,549,554,556,560,561,565,567,568,569,572,573,575,576,577,579,582,585,586,589,590,596,600,602,604,607,611,612,613,614,616,618,620,623,625,627,629,630,631,633,635,636,642,643,646,648,650,651,652,653,656,662,664,665,667,669,671,675,677,682,683,684,685,688,698,702,707,709,711,725,727,729,731,736,740,741,743,744,751,752,757,758,760,761,763,765,767,768,769,774,777,778,781,783,784,785,786,787,797,798,800,809,823,827,828,829,830,831,838,840,841,844,846,848,849,851,852,854,856,859,860,862,864,866,868,873,875,877,878,882,883,884,885,887,888,889,893,900,903,908,910,916,917,921,922,924,927,929,930,931,932,933,935,936,940,942,943,944,945,947,951,955,963,965,970,972,976,978,982,983,984,985,987,989,990,992,993,997,998,999,1000,1001,1004,1010,1011,1014,1015
# Noisy channels on Top Sensor : 2,5,8,13,18,19,20,22,23,24,29,31,33,35,49,50,53,54,58,59,62,65,66,67,68,70,80,83,85,87,92,93,94,98,102,103,104,106,109,112,113,117,119,130,132,133,134,135,137,138,141,142,144,146,149,154,155,156,159,160,161,164,165,167,169,176,177,178,179,181,183,185,186,189,190,191,193,194,195,196,197,205,206,207,213,219,224,225,232,236,237,238,239,240,241,242,243,244,246,252,253,256,261,263,267,269,273,276,282,284,287,295,297,305,307,308,315,317,319,322,323,325,326,329,332,334,338,345,350,354,359,361,367,368,369,370,371,373,376,377,380,384,387,392,393,394,397,399,400,401,409,410,413,418,421,422,423,424,427,429,431,432,435,437,438,440,443,445,448,450,452,455,457,459,460,461,465,476,479,481,482,484,486,488,491,493,497,499,501,504,506,507,508,511,515,517,521,525,528,531,533,538,539,541,543,544,547,548,549,550,554,556,557,563,566,568,569,572,575,578,579,584,587,588,589,591,592,594,595,597,602,603,605,607,609,615,616,617,618,619,623,627,629,630,641,644,647,649,651,653,654,655,660,665,674,680,684,692,694,696,698,704,709,710,713,718,719,721,730,731,733,737,741,743,744,745,748,749,751,752,754,756,757,762,763,764,765,766,768,769,770,771,774,776,782,785,786,789,798,802,803,813,815,818,821,823,826,829,830,835,836,837,838,846,847,849,852,853,854,855,857,861,869,873,875,889,890,892,896,901,902,905,906,907,910,915,918,921,926,928,932,936,939,940,942,946,947,950,952,954,956,959,960,963,967,968,969,973,975,976,980,981,985,986,989,993,994,995,997,1004,1005,1007
# Dead channels on Bottom Sensor : 
# Dead channels on Top Sensor : 
08.06.2019 09:35 ||I| Results saved!
(Noise) Occupancy measurement finished at: Sat Jun  8 09:35:28 2019
	elapsed time: 1.37606 seconds
Complete system test finished at: Sat Jun  8 09:35:28 2019
	elapsed time: 13.1926 seconds
08.06.2019 09:35 ||I| [1m[31mclosing result file![0m
08.06.2019 09:35 ||I| [1m[31mDestroying memory objects[0m
User comment: 
